from time import time
from math import floor
from disco.bot import Plugin

import requests
import json
import re
import wikia
import embeds
import util


class Commands(Plugin):

    def __init__(self, bot, config):
        super(Commands, self).__init__(bot, config)
        self.container = util.Container()
        with open('./data/fish.json', 'r') as f:
            self.container.fish = json.load(f)
        with open('./data/disposition.json', 'r') as f:
            self.container.disposition = json.load(f)

    @Plugin.command('market', '<item:str...>', aliases=['m'])
    def command_market(self, event, item):
        item = self._parse_item(item)
        try:
            if item['limit'] > 9:
                raise ValueError('The maximum amount of orders you can view is 9!')
            r = requests.get('https://api.warframe.market/v1/items/' + item['name'].lower().replace(' ', '_') + '/orders?include=item')
            r.raise_for_status()
            item_data = r.json()
            orders = item_data['payload']['orders']
            orders = [_ for _ in orders if _['user']['status'] == 'ingame' and _['order_type'] == 'sell'
                      and _['user']['region'] == 'en']
            orders.sort(key=lambda k: k['platinum'])
            orders = orders[:item['limit']]
            if orders:
                is_mod = True if 'mod' in item_data['include']['item']['items_in_set'][0]['tags'] else False
                for order in orders:
                    event.msg.reply(embed=embeds.MarketEmbed(item, item_data['include']['item']['items_in_set'][0]['icon'], order, is_mod))
            else:
                raise ValueError('There are no orders for the item you requested!')
        except requests.HTTPError:
            event.msg.reply(embed=embeds.ErrorEmbed('Failed to get market data!'))
        except ValueError as e:
            event.msg.reply(embed=embeds.ErrorEmbed(e))

    @Plugin.command('poet', aliases=['cycle', 'c'])
    def command_poet(self, event):
        renew = False
        while True:
            worldstate = self._get_worldstate() if renew is False else self._get_worldstate(True)
            syndicates = worldstate['SyndicateMissions']
            cetus = [_ for _ in syndicates if _['Tag'] == 'CetusSyndicate']
            if not cetus:
                renew = True
                continue
            activation = int(cetus[0]['Activation']['$date']['$numberLong'])
            expiry = int(cetus[0]['Expiry']['$date']['$numberLong'])
            cycle_time = expiry - activation
            now = round(time() * 1000)
            if now > expiry:
                renew = True
                continue
            break
        seconds_past = now - activation
        total_day_time = cycle_time - floor(cycle_time * 0.33333333333)
        if seconds_past < total_day_time:
            seconds_left = total_day_time - seconds_past
            cycle = 'Day'
            next_cycle = 'Time until night'
        else:
            seconds_left = cycle_time - seconds_past
            cycle = 'Night'
            next_cycle = 'Time until day'
        seconds_left = util.pretty_time(floor(seconds_left/1000))
        event.msg.reply(embed=embeds.CycleEmbed(cycle, next_cycle, seconds_left))

    @Plugin.command('wiki', '<q:str...>')
    def command_wiki(self, event, q):
        try:
            search = wikia.search('warframe', q, 1)
            article = wikia.page('warframe', None, search['id'])
            embed = embeds.WikiEmbed(article)
        except wikia.WikiaError as e:
            embed = embeds.ErrorEmbed(e)
        event.msg.reply(embed=embed)

    @Plugin.command('fish', '<fish:str...>')
    def command_fish(self, event, fish):
        fish = fish.strip().lower()
        re_obj = re.compile('^.*{}.*$'.format(fish))
        fish = None
        try:
            for key in self.container.fish.keys():
                if re_obj.match(key):
                    fish = self.container.fish[key]
                    break
            if fish is None:
                raise ValueError('There is no such fish!')
            embed = embeds.FishEmbed(fish)
        except ValueError as e:
            embed = embeds.ErrorEmbed(e)
        event.msg.reply(embed=embed)

    @Plugin.command('dispo', '<weapon:str...>', aliases=['disposition'])
    def command_dispo(self, event, weapon):
        weapon = weapon.strip().lower()
        re_obj = re.compile('^{}$'.format(weapon))
        weapon = None
        try:
            for key in self.container.disposition.keys():
                if re_obj.match(key):
                    weapon = self.container.disposition[key]
                    break
            if weapon is None:
                raise ValueError('Weapon not found!')
            embed = embeds.DispoEmbed(weapon)
        except ValueError as e:
            embed = embeds.ErrorEmbed(e)
        event.msg.reply(embed=embed)

    @Plugin.command('clear', '<limit:int>')
    def command_clear(self, event, limit):
        if util.check_owner(event):
            messages = self.client.api.channels_messages_list(event.channel.id, None, None, None, limit)
            delete_list = []
            for message in messages:
                delete_list.append(message.id)
            self.client.api.channels_messages_delete_bulk(event.channel.id, delete_list)
            event.msg.delete()

    @Plugin.command('eval')
    def command_eval(self, event):
        if util.check_owner(event):
            eval(event.codeblock)
        else:
            event.msg.reply('This command is only available to bot owner')

    @Plugin.command('help')
    def command_help(self, event):
        embed = embeds.HelpEmbed()
        event.author.open_dm().send_message(embed=embed)
        if not event.channel.is_dm:
            event.msg.reply('Check DM!')

    def _parse_item(self, item):
        if item != 'last':
            m = re.search(r'\d+$', item)
            if m:
                limit = int(m.group(0))
                if limit < 3:
                    limit = 3
                item = item[:-1]
            else:
                limit = 3
            self.container.item = {'name': item.strip().title(), 'limit': limit}
            return self.container.item
        return self.container.item

    def _get_worldstate(self, renew=False):
        if renew or self.container.worldstate is None:
            worldstate = requests.get('http://content.warframe.com/dynamic/worldState.php').json()
            self.container.worldstate = worldstate
            return worldstate
        return self.container.worldstate
