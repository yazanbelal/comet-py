from disco.bot import Plugin
from disco.types.user import Game, GameType, Status
from util.util import check_owner, get_owner_id


class Core(Plugin):

    @Plugin.listen('Ready')
    def on_ready(self, event):
        self.client.update_presence(Status.online, Game(type=GameType.default, name=':help'))

    @Plugin.listen('GuildMemberAdd')
    def on_ready(self, event):
        if event.guild.id == 336162126483685376:
            self.client.api.guilds_members_roles_add(event.guild.id, event.user.id, 370278285541900298)
        elif event.guild.id == 465639640184127488:
            self.client.api.guilds_members_roles_add(event.guild.id, event.user.id, 465642265113657356)
        else:
            return

    @Plugin.command('play', '<status:str...>')
    def command_play(self, event, status):
        if check_owner(event):
            self.client.update_presence(Status.online, Game(type=GameType.default, name=status))

    @Plugin.command('reload', '<plugin:str...>')
    def command_reload(self, event, plugin):
        if check_owner(event):
            self.bot.plugins.get(plugin.title()).reload()
            self.client.api.users_me_dms_create(get_owner_id()).send_message('{} reloaded'.format(plugin))
