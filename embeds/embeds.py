from disco.types.message import MessageEmbed


EMBED_COLOR = 0x4dffc3


class MarketEmbed(MessageEmbed):
    def __init__(self, item, item_icon, order, is_mod):
        super(MarketEmbed, self).__init__()
        self.title = item['name']
        self.url = 'https://warframe.market/items/{}'.format(item['name'].lower().replace(' ', '_'))
        '''
        self.set_thumbnail(
            url='https://warframe.market/static/assets/{}'.format(item_icon),
            width=240,
            height=240
        )
        '''
        self.add_field(name='Name', value=order['user']['ingame_name'], inline=True)
        self.add_field(name='Price', value="{} Platinum".format(order['platinum']), inline=True)
        self.add_field(name='Quantity', value=order['quantity'], inline=True)
        if is_mod:
            self.add_field(name='Rank', value=order['mod_rank'], inline=True)
        self.add_field(name='Message', value="/w {} WTB {} {}p".format(order['user']['ingame_name'], item['name'], order['platinum']), inline=True)
        self.color = EMBED_COLOR
        self.set_footer(text='Source warframe.market')


class WikiEmbed(MessageEmbed):
    def __init__(self, article):
        super(WikiEmbed, self).__init__()
        self.title = article.title
        self.description = article.summary
        self.url = article.url.replace(' ', '_')
        self.color = EMBED_COLOR
        if article.images:
            self.set_image(url=article.images[0])


class CycleEmbed(MessageEmbed):
    def __init__(self, cycle, next_cycle, seconds_left):
        super(CycleEmbed, self).__init__()
        self.title = 'Plains of Eidolon Cycle'
        self.add_field(name='Currently it is', value=cycle, inline=True)
        self.add_field(name=next_cycle, value=seconds_left, inline=True)
        self.color = 0x4dffc3


class FishEmbed(MessageEmbed):
    def __init__(self, fish):
        super(FishEmbed, self).__init__()
        self.title = fish['name']
        self.set_thumbnail(url=fish['img'])
        self.add_field(name='Location', value=fish['location'], inline=True)
        self.add_field(name='Time', value=fish['time'], inline=True)
        self.add_field(name='Spear', value=fish['spear'], inline=True)
        self.add_field(name='Rarity', value=fish['rarity'], inline=True)
        self.add_field(name='Bait', value=fish['bait'], inline=True)
        self.add_field(name='Produces', value=fish['produces'], inline=True)
        self.color = EMBED_COLOR


class DispoEmbed(MessageEmbed):
    def __init__(self, weapon):
        super(DispoEmbed, self).__init__()
        self.title = weapon['name']
        if weapon['img']:
            self.set_thumbnail(url=weapon['img'])
        self.description = 'Riven Disposition: {}/5'.format(weapon['dispo'])
        self.url = weapon['url']
        self.color = EMBED_COLOR


class HelpEmbed(MessageEmbed):
    def __init__(self):
        super(HelpEmbed, self).__init__()
        self.title = 'Comet Help'
        self.description = '**Note**: <> means required and [] means optional in the arguments'
        self.add_field(name=':market <item> [orders]', value=(
            'Get sell orders for an item from [Warframe Market](http://warframe.market)'
            '\nyou can also choose how many orders to show after the item name (default/min is 3 and max is 9)'
        )
                        )
        self.add_field(name=':poet', value='Get Plains of Eidolon current cycle and its remaining time.')
        self.add_field(name=':wiki <query>', value='Search Warframe Wiki.')
        self.add_field(name=':dispo <weapon>', value='Get a weapon riven disposition by name.')
        self.add_field(name=':fish <fish>', value='Get fish information by name.')
        self.add_field(name=':help', value='Get this help message.')
        self.color = EMBED_COLOR


class ErrorEmbed(MessageEmbed):
    def __init__(self, error):
        super(ErrorEmbed, self).__init__()
        self.description = str(error)
        self.color = 0xff0000
