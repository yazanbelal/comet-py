def pretty_time(seconds):
    seconds = int(seconds)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if hours > 0:
        return '{}h {}m {}s'.format(hours, minutes, seconds)
    elif minutes > 0:
        return '{}m {}s'.format(minutes, seconds)
    return '{}s'.format(seconds)


def check_owner(event):
    if event.msg.author.id == get_owner_id():
        return True
    return False


def get_owner_id():
    return 262366547882803210


def timer(time, func, args=None):
    from threading import Timer
    t = Timer(time, func, args)
    t.start()


def delete_message(message):
    message.delete()
