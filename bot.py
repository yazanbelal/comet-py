from __future__ import print_function

import logging

from gevent import monkey

monkey.patch_all()


def bot_main(run=False):
    from disco.client import Client, ClientConfig
    from disco.bot import Bot, BotConfig
    from disco.util.logging import setup_logging

    setup_logging(level=logging.INFO)

    config = ClientConfig().from_file('config.yaml')
    client = Client(config)
    bot_config = BotConfig(config.bot)
    bot = Bot(client, bot_config)
    if run:
        bot.run_forever()
    return bot


if __name__ == '__main__':
    bot_main(True)
