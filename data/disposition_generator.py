import json
import re


with open('weapondata.json', 'r') as f:
    weapons = json.load(f)

weapons_dispositions = {}
weapons_types = {'Primary', 'Secondary', 'Melee'}
regex = re.compile(r'/revision/.*')
for weapon in weapons:
    if weapon['type'] in weapons_types:
        try:
            weapons_dispositions[weapon['name'].lower()] = {
                'dispo': weapon['riven_disposition'],
                'name': weapon['name'],
                'url': weapon['url'],
                'img': regex.sub('', weapon.get('thumbnail', ''))
            }
        except KeyError:
            print(weapon)
with open('disposition.json', 'w+') as f:
    json.dump(weapons_dispositions, f)
